const mongoose = require('mongoose');


const TodoSchema = mongoose.Schema({
    content: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    owner: {
        type: String,
        required: true,
    },
    created_at:{
        type: Number,
        required: true,
    }
});

module.exports = mongoose.model('Todo',TodoSchema);