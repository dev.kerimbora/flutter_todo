const express = require('express');
const Todo = require('../models/Todo');

const router = express.Router();

const {
    todo_eCode1,
    todo_eCode2,
    todo_eCode3,
    todo_eCode4,
    todo_eCode5,
    todo_eCode6,
} = require('../errors/todo_route_errors');

router.get('/',(req,res)=>{
    res.send('todos home');
});



router.get('/myTodos',(req,res)=>{
    if(req.query.email === undefined){ 
        return  res.status(403).json(todo_eCode1);
    }


    getMyTodos()
    .then(myTodos=>{
        return  res.status(200).json(myTodos);
    })
    .catch(error=>{
        console.log(error);
        return res.status(403).json(unknowError(error));
    });



    async function getMyTodos(){
        try{
            const todoList = await Todo.find({owner: req.query.email}).sort({created_at: -1});
            return todoList;
        }catch(e){
            throw new Error(e);
        }
    }    

});


router.post('/create',(req,res)=>{

    if(req.body.content == undefined) return res.status(403).json(todo_eCode2);
    if(req.body.category == undefined) return res.status(403).json(todo_eCode3);
    if(req.body.author == undefined) return res.status(403).json(todo_eCode4);
    if(req.body.owner == undefined) return res.status(403).json(todo_eCode5);


    const todo = new Todo({
        content: req.body.content,
        category: req.body.category,
        author: req.body.author,
        owner: req.body.owner,
        created_at: Math.floor(new Date().getTime()/1000.0)
    });



    createTodo()
    .then(result=>{
        return  res.status(200).json({
            message: "Todo Created",
            todo: result
        });
    })
    .catch(error=>{
        console.log(error);
        return res.status(403).json(unknowError(error));
    });

 

    async function createTodo(){
        try{
            const resultData = await todo.save();
            return resultData;
        }catch(e){
            throw new Error(e);
        }
    }    

})


router.delete('/remove/:todoId',(req,res)=>{

    if(req.params.todoId === undefined) return res.status(403).json(todo_eCode6);

    Todo.remove({_id: req.params.todoId})
        .then(result=>{
            return res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            return res.status(403).json(unknowError(err));
        })
})


module.exports = router;
