const express = require('express');
const User = require('../models/User');
const router = express.Router();
const {
    unknowError,
    user_eCode1,
    user_eCode2,
    user_eCode3,
    user_eCode4
} = require('../errors/user_route_errors');



router.get('/', (req,res)=>{
    User.find().then(data=>{
        let userList = [];
        data.forEach(userDoc => {
            userList.push(userDoc.email)
        });
        return res.status(200).json(userList);
    }).catch(err=>{
        console.log(err);
        return res.status(403).json(unknowError(err));
    })
});


router.post('/register', (req,res)=>{

    if(req.body.email === undefined){
        return  res.status(403).json(user_eCode1);
    }

    if(req.body.password === undefined){
        return res.status(403).json(user_eCode2);
    }

    const user = new User({
        email: req.body.email,
        password: req.body.password
    });



    registerUser()
        .then(data=>{
            return  res.status(200).json({
                message: "User Created",
                data
            });
        })
        .catch(error=>{
            console.log(error);
            return res.status(403).json(unknowError(error));
        });

    async function registerUser(){
        try{

            const userListWithQuery = await User.find({email:req.body.email}).exec();
    
            if(userListWithQuery.length == 0){
                const resultData = await user.save();
                return resultData;
            }else{
                return res.status(201).json(user_eCode3);
            }
        }catch(e){
            throw new Error(e);
        }
    }

});


router.post('/login',(req,res)=>{

    if(req.body.email === undefined){
        return res.status(403).json(user_eCode1);
    }

    if(req.body.password === undefined){
        return res.status(403).json(user_eCode2);
    }

    const user = new User({
        email: req.body.email,
        password: req.body.password
    });


    loginUser()
        .then(user=>{
            return res.status(200).json({
                message: "User Loggined",
                userData: user
            });
        })
        .catch(error=>{
            return res.status(403).json(unknowError(error));
        });

    async function loginUser(){
        try{

            const userQuery = await User.findOne({
                email:req.body.email,
                password: req.body.password
            }).exec();

            if(userQuery == null){
                return  res.status(403).json(user_eCode4); 
            }else{
                return userQuery;
            }
        }catch(e){
            throw new Error(e);
        }
    }


});



module.exports = router;
