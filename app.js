const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');

const app = express();

app.use(bodyParser.json());

// Import Routers
const usersRoute = require('./routers/users');
const todosRoute = require('./routers/todos');


//Routes
app.get('/',(req,res)=>{
    res.send('Flutter ToDo DetaySoft API v1.0');
});

app.use('/user',usersRoute);
app.use('/todo',todosRoute);



//Connect MongoDb
mongoose.connect(process.env.MONGODB_URL,()=>{
    console.log("Connected  MongoDB");
});



// Start Listening Server
app.listen(process.env.PORT || 5000);
