
const unknowError = (error)=>{
    return {
        e_code: 0,
        message: "unknow error",
        error
    };
};

const user_eCode1 = {
    e_code: "user_eCode1",
    message: "email not be undefined"
};

const user_eCode2 = {
    e_code: "user_eCode2",
    message: "password not be undefined"
};

const user_eCode3 = {
    e_code: "user_eCode3",
    message: "user registered"
};
2
const user_eCode4 = {
    e_code: "user_eCode4",
    message: "user not found"
};



module.exports = {
    unknowError,
    user_eCode1,
    user_eCode2,
    user_eCode3,
    user_eCode4
}