
const unknowError = (error)=>{
    return {
        e_code: 0,
        message: "unknow error",
        error
    };
};

const todo_eCode1 = {
    e_code: "todo_eCode1",
    message: "email not be undefined"
};



const todo_eCode2 = {
    e_code: "todo_eCode2",
    message: "content not be undefined"
};

const todo_eCode3 = {
    e_code: "todo_eCode3",
    message: "category not be undefined"
};

const todo_eCode4 = {
    e_code: "todo_eCode4",
    message: "author not be undefined"
};

const todo_eCode5 = {
    e_code: "todo_eCode5",
    message: "owner not be undefined"

};


const todo_eCode6 = {
    e_code: "todo_eCode6",
    message: "id not be undefined"
};


module.exports = {
    unknowError,
    todo_eCode1,
    todo_eCode2,
    todo_eCode3,
    todo_eCode4,
    todo_eCode5,
    todo_eCode6
}